/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2022
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

// STL includes
#ifdef WIN32
#	include <conio.h>
#else
#	include <ncurses.h>
#endif

#include <iostream>
#include <signal.h>

// ITA includes
#include <ITAException.h>
#include <ITAFileSystemUtils.h>
#include <ITANumericUtils.h>
#include <ITAStringUtils.h>

// VA includes
#include "VAServer_instrumentation.h"

#include <CLI/CLI.hpp>
#include <VA.h>
#include <VACore.h>
#include <VANet.h>
#include <Windows.h>
#include <chrono>
#include <filesystem>
#include <regex>
#include <spdlog/fmt/bundled/chrono.h>
#include <spdlog/fmt/bundled/ostream.h>
#include <thread>
#include <mutex>

using namespace std;

//--- Global Constants ---
const std::string CLI_SERVERADDRESS_OPTION = "server-address";
const std::string CLI_CONFIGFILE_OPTION = "config-file";

//--- Global Variables ---

std::unique_ptr<IVAInterface> pCore                      = nullptr;
std::unique_ptr<IVANetServer> pServer                    = nullptr;
std::unique_ptr<IVAEventHandler> pServerCoreEventHandler = nullptr;

std::atomic_bool abShutdown = false;
std::mutex oVACoreObjectsMutex;

#if VASERVER_DEVELOPER_MODE
auto now                 = std::chrono::system_clock::now( );
std::string sProfileFile = fmt::format( "VAServer{:%FT%H-%M-%S}.prof", now );
#endif


//--------- Start Arguments ----------
//------------------------------------

// Class representing the start arguments of the VAServer
struct CVAServerArguments
{
	std::string sVACoreConfigPath { "conf/VACore.ini" };
	std::string sServerAddress { "0.0.0.0:12340" };
	std::string sLogFile { "VAServer.log" };
	std::string sLogConfig { };
	bool bRemoteControlMode        = false;
	bool bIgnoreDefaultSearchPaths = false;
	bool bAddressSetByUser         = false;
	bool bDefaultConfigUsed        = false;
#if VASERVER_DEVELOPER_MODE
	bool bStreamProfile = false;
	bool bDumpProfile   = false;
#endif
};


//--- Helper Function Declaration ---
//-----------------------------------

void DefineCLI( CLI::App& app, CVAServerArguments& oStartArguments );
std::string GetVAServerHelp( );

IHTA::Instrumentation::LoggerRegistry::SinkContainer InitLogger( const CVAServerArguments& oStartArguments );
void InitProfiler( const CVAServerArguments& oStartArguments );

void DisplayInitiationMessages( const CVAServerArguments& oStartArguments );
void DisplayControlMessages( const CVAServerArguments& oStartArguments );

//Loads given config file or uses internal defaults if given empty filepath. Appends default paths to config.
CVAStruct LoadConfig( const CVAServerArguments& oStartArguments, const std::filesystem::path& fpVAServerExe );
// Returns a list of default paths based on the VAServerExe path, returns empty list if default paths are to be ignored
std::vector<std::filesystem::path> GetDefaultSearchPaths( const std::filesystem::path& fpVAServerExe, bool bIgnoreDefaultPaths );
//Returns config struct with internal defaults
CVAStruct GetDefaultConfig( const std::vector<std::filesystem::path>& voDefaultSearchPaths = std::vector<std::filesystem::path>( 0 ) );

//Creates VACore instance using given config and connects event handler if required
void CreateVACore( const CVAStruct& oConfig, const bool bRemoteControlMode );
//Creates server instance and establishes network connection
bool EstablishNetworkConnection( const std::string& sServerAddress );

//Starts main loop waiting on shutdown request. If remote control is disabled reading keyboard inputs
void WaitForShutdownRequest( const CVAServerArguments&, IHTA::Instrumentation::LoggerRegistry::SinkContainer& );
//Reads keyboard input and applies changes to VACore for defined keys (see DisplayControlMessages())
void ControlKeyboardInput( IHTA::Instrumentation::LoggerRegistry::SinkContainer& );
//Converts a log level from VA to spdlog
spdlog::level::level_enum ConvertLogLevel( int iLevel );

// Stops the VAServer and cleans up server and core instances as well as event handlers
void StopServer( );



//----- Signal-/Event- Handling -----
//-----------------------------------

// Class handling events from VACore within the VAServer
class CServerCoreEventHandler : public IVAEventHandler
{
public:
	inline void HandleVAEvent( const CVAEvent* pEvent )
	{
		if( pEvent->iEventType == CVAEvent::SHOTDOWN_REQUEST )
			abShutdown = true;
	};
};
// Function handling events for terminating the program
void InterruptSignalHandler( int iSignum )
{
	abShutdown = true;
	StopServer();
	std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) ); //To make sure Core is cleaned up properly (not clear if really necessary).
}
// Function registering signals interrupting the program
void RegisterInterruptSignals( )
{
	signal( SIGABRT, InterruptSignalHandler );
	signal( SIGBREAK, InterruptSignalHandler ); // closing window
	signal( SIGTERM, InterruptSignalHandler );
	signal( SIGINT, InterruptSignalHandler ); // CTRL+C etc.
}
#if VASERVER_DEVELOPER_MODE
void atexit_handler( )
{
	auto iStoredBlocks = profiler::dumpBlocksToFile( sProfileFile.c_str( ) );
	VASERVER_INFO( "[{:<20}] Stored {} blocks.", "Profiling", iStoredBlocks );
}
#endif

//-------- Main Function --------
//-------------------------------

int main( int argc, char* argv[] )
{
	CVAServerArguments oStartArguments;

	//--- CLI Definition + Parsing Arguments ----
	//-------------------------------------------
	{
		CLI::App app { GetVAServerHelp( ), "VAServer" };
		DefineCLI( app, oStartArguments );
		CLI11_PARSE( app, argc, argv );

		// Dependend CLI arguments
		auto pAddressOption                  = app.get_option( CLI_SERVERADDRESS_OPTION );
		auto pConfigOption                   = app.get_option( CLI_CONFIGFILE_OPTION );
		oStartArguments.bAddressSetByUser    = pAddressOption->count( ) > 0;
		
		const bool bConfigFileSetByUser = pConfigOption->count( ) > 0;
		if( !bConfigFileSetByUser )
		{
			const std::string sDefaultConfigFileMain = oStartArguments.sVACoreConfigPath; // If started from VA main folder
			const std::string sDefaultConfigFileBin  = "../" + sDefaultConfigFileMain;    // If started from bin folder (location of VAServer.exe)
			const std::string sDefaultConfigFileDev  = "../../" + sDefaultConfigFileMain; // If started from IDE (Developers only)
			oStartArguments.sVACoreConfigPath        = "";                                // reset for following logic
			oStartArguments.bDefaultConfigUsed       = true;
			if( std::filesystem::exists( sDefaultConfigFileMain ) )
				oStartArguments.sVACoreConfigPath = sDefaultConfigFileMain;
			else if( std::filesystem::exists( sDefaultConfigFileBin ) )
				oStartArguments.sVACoreConfigPath = sDefaultConfigFileBin;
			else if( std::filesystem::exists( sDefaultConfigFileDev ) )
				oStartArguments.sVACoreConfigPath = sDefaultConfigFileDev;
			else
				oStartArguments.bDefaultConfigUsed = false;
		}
	}

	try
	{
		IHTA::Instrumentation::LoggerRegistry::SinkContainer oLoggerSinks = InitLogger( oStartArguments );
		InitProfiler( oStartArguments );

		DisplayInitiationMessages( oStartArguments );

		const std::filesystem::path fpVAServerExe = std::filesystem::absolute( argv[0] );
		const CVAStruct oConfig                   = LoadConfig( oStartArguments, fpVAServerExe );
		CreateVACore( oConfig, oStartArguments.bRemoteControlMode );
		if( !EstablishNetworkConnection( oStartArguments.sServerAddress ) )
			return 255;

		RegisterInterruptSignals( );
		DisplayControlMessages( oStartArguments );

		WaitForShutdownRequest( oStartArguments, oLoggerSinks );
		StopServer( );
	}
	catch( CVAException& e )
	{
		VASERVER_CRITICAL( "[{:<20}] {}", "Exception", e );

		StopServer( );

		return 255;
	}
	catch( ... )
	{
		VASERVER_CRITICAL( "[{:<20}] An unknown error occured", "Exception" );

		StopServer( );

		return 255;
	}

	return 0;
}


//--- Helper Function Definitions ---
//-----------------------------------

void DefineCLI( CLI::App& app, CVAServerArguments& oStartArguments )
{
	app.set_version_flag( "--version,-v",
	                      []( )
	                      {
		                      auto pCore = std::unique_ptr<IVAInterface>( VACore::CreateCoreInstance( GetDefaultConfig( ) ) );
		                      CVAVersionInfo ver;
		                      pCore->GetVersionInfo( &ver );
		                      return ver.ToString( );
	                      } );

	app.add_option( CLI_SERVERADDRESS_OPTION + ",--server-address,-s", oStartArguments.sServerAddress, "Address of the server, in the from (address):(port)" )
	                       ->expected( 1 )
	                       ->check(
	                           []( const std::string& arg )
	                           {
		                           int del = arg.find( ':' );
		                           if( del == std::string::npos )
		                           {
			                           return std::string( "not in correct format" );
		                           }

		                           auto port = arg.substr( del + 1, std::string::npos );

		                           auto port_valid = CLI::Range( 1024, 49151 )( port );

		                           if( !port_valid.empty( ) )
		                           {
			                           return "Port: " + port_valid;
		                           }
		                           return std::string( );
	                           } )
	                       ->capture_default_str( )
	                       ->type_name( "ADDR:PORT" );
	app.add_option( CLI_CONFIGFILE_OPTION + ",--config,-c", oStartArguments.sVACoreConfigPath, "Core configuration ini file" )
	                         ->expected( 1 )
	                         ->type_name( "FILE" )
	                         ->capture_default_str( );

	app.add_flag( "--remote,-r", oStartArguments.bRemoteControlMode, "Start server in remote control mode" ); // remote_control
	app.add_flag( "--ignore-default-paths,-i", oStartArguments.bIgnoreDefaultSearchPaths, "Do not add 'data' and 'conf' folders to search path." );

	app.add_option( "--log-file,-l", oStartArguments.sLogFile, "File in which to save the log, -1 for no file log" )
	    ->type_name( "FILE" )
	    ->capture_default_str( )
	    ->transform(
	        []( const std::string& arg )
	        {
		        if( arg == "-1" )
		        {
			        return std::string { };
		        }
		        return arg;
	        } );
	app.add_option( "--log-config,-x", oStartArguments.sLogConfig, "Configuration for spdlog logger levels, format: [global level][,(logger name)=(level)]*" );

#if VASERVER_DEVELOPER_MODE
	app.add_flag( "--stream-profile,-S", oStartArguments.bStreamProfile, "Start listening for profiler GUI 'start-capture-signal'" );
	app.add_flag( "--dump-profile,-D", oStartArguments.bDumpProfile, "Dump profile to file given by '--profile-file'" );
	app.add_option( "--profile-file,-F", sProfileFile, "File to store the profile dump" )->type_name( "FILE" )->capture_default_str( );
#endif
}
std::string GetVAServerHelp( )
{
	return R"(
  VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
   VVV      VVV AAA
    VVV    VVV   AAA
     VVV  VVV     AAA        Copyright 2015-2024
      VVVVVV       AAA       Institute for Hearing Technology and Acoustics (IHTA)
       VVVV         AAA      RWTH Aachen University

VAServer is a lightweight command line C++ server application for real-time auralization.
The configuration of the application is done in an '.ini' file which can be found under the 'conf' folder.

For more information, especially for all configuration options visit https://www.virtualacoustics.de/VA/documentation/ .
	)";
}


IHTA::Instrumentation::LoggerRegistry::SinkContainer InitLogger( const CVAServerArguments& oStartArguments )
{
	std::smatch m;
	const auto bGlobalLogLevelVerbose = std::regex_search( oStartArguments.sLogConfig, m, std::regex( "^(trace)|(debug)" ) );
	const auto sLogConfigArg          = "SPDLOG_LEVEL=" + oStartArguments.sLogConfig;
	std::array<const char*, 2> vSPDLOGArgv( { "", sLogConfigArg.c_str( ) } );
	IHTA::Instrumentation::LoggerRegistry::init_logging( 2, vSPDLOGArgv.data( ) );
	IHTA::Instrumentation::LoggerRegistry::DefaultSinksConfig oSinkConfig;
	oSinkConfig.console_sink_format = IHTA::Instrumentation::LogFormat::va_like;
	oSinkConfig.log_file_name       = oStartArguments.sLogFile;
	if( bGlobalLogLevelVerbose )
	{
		oSinkConfig.console_sink_level = spdlog::level::from_str( m[0].str( ) );
	}
	auto oLoggerSinks = IHTA::Instrumentation::LoggerRegistry::create_default_sinks( oSinkConfig );

	if( oSinkConfig.log_file_name.empty( ) )
	{
		VASERVER_WARN( "[{:<20}] No file log requested, logs will not be saved", "Initiation" );
	}
	else
	{
		VASERVER_INFO( "[{:<20}] Log will be saved to '{}'", "Initiation", oSinkConfig.log_file_name );
	}
	return oLoggerSinks;
}
void InitProfiler( const CVAServerArguments& oStartArguments )
{
#if VASERVER_DEVELOPER_MODE
	if( oStartArguments.bDumpProfile )
	{
		profiler::startCapture( );

		std::atexit( atexit_handler );
	}

	if( oStartArguments.bStreamProfile )
	{
		profiler::startListen( );
	}
#endif
}

void DisplayInitiationMessages( const CVAServerArguments& oStartArguments )
{
	if( !oStartArguments.bAddressSetByUser )
	{
		VASERVER_INFO( "[{:<20}] No bind address and port given, using default: {}", "Initiation", oStartArguments.sServerAddress );
	}
	if( oStartArguments.bDefaultConfigUsed )
	{
		VASERVER_INFO( "[{:<20}] Using default configuration file '{}'", "Initiation", oStartArguments.sVACoreConfigPath );
	}
}
void DisplayControlMessages( const CVAServerArguments& oStartArguments )
{
	if( oStartArguments.bRemoteControlMode )
	{
		VASERVER_INFO( "[{:<20}] Enering remote control mode. Type CTRL+C to stop server manually or send shutdown message from client", "Control" );
		std::cout << std::endl;
	}
	else
	{
		VASERVER_INFO( "[{:<20}] Controls:", "Control" );
		VASERVER_INFO( "[{:<20}]    m    Toggle output muting", "Control" );
		VASERVER_INFO( "[{:<20}]    +    Increase output gain by 3dB", "Control" );
		VASERVER_INFO( "[{:<20}]    0    Output gain 0dB", "Control" );
		VASERVER_INFO( "[{:<20}]    -    Decrease output gain by 3dB", "Control" );
		VASERVER_INFO( "[{:<20}]    r    Reset core", "Control" );
		VASERVER_INFO( "[{:<20}]    l    Circulate log levels", "Control" );
		VASERVER_INFO( "[{:<20}]    c    List connected clients", "Control" );
		VASERVER_INFO( "[{:<20}]    s    List search paths", "Control" );
		VASERVER_INFO( "[{:<20}]    q    Quit", "Control" );
		std::cout << std::endl;
	}
}


CVAStruct LoadConfig( const CVAServerArguments& oStartArguments, const std::filesystem::path& fpVAServerExe )
{
	const auto voDefaultSearchPaths = GetDefaultSearchPaths( fpVAServerExe, oStartArguments.bIgnoreDefaultSearchPaths );
	if( oStartArguments.sVACoreConfigPath.empty( ) )
	{
		VASERVER_WARN( "[{:<20}] No configuration file found using internal defaults", "Load config", oStartArguments.sVACoreConfigPath );
		return GetDefaultConfig( voDefaultSearchPaths );
	}
	else
	{
		VASERVER_INFO( "[{:<20}] Core configuration path: {}", "Load config", correctPath( oStartArguments.sVACoreConfigPath ) );
		return VACore::LoadCoreConfigFromFile( oStartArguments.sVACoreConfigPath, voDefaultSearchPaths );
	}
}
std::vector<std::filesystem::path> GetDefaultSearchPaths( const std::filesystem::path& fpVAServerExe, bool bIgnoreDefaultPaths )
{
	std::vector<std::filesystem::path> voSearchPaths;
	if( bIgnoreDefaultPaths )
		return voSearchPaths;

	auto fpVAMainPath = fpVAServerExe.parent_path( ).parent_path( );
	if( !std::filesystem::exists( fpVAMainPath / "conf" ) ) // Started from IDE / Dev mode
		fpVAMainPath = fpVAMainPath.parent_path( );

	const auto fpConfPath = fpVAMainPath / "conf";
	const auto fpDataPath = fpVAMainPath / "data";

	if( std::filesystem::exists( fpConfPath ) )
	{
		VASERVER_INFO( "[{:<20}] Adding default config directory to config", "Default paths" );
		voSearchPaths.push_back( fpConfPath );
	}
	else
		VASERVER_WARN( "[{:<20}] Could not find default config directory", "Default paths" );
	if( std::filesystem::exists( fpDataPath ) )
	{
		VASERVER_INFO( "[{:<20}] Adding default data directory to config", "Default paths" );
		voSearchPaths.push_back( fpDataPath );
	}
	else
		VASERVER_WARN( "[{:<20}] Could not find default data directory", "Default paths" );

	return voSearchPaths;
}
CVAStruct GetDefaultConfig( const std::vector<std::filesystem::path>& voDefaultSearchPaths )
{
	CVAStruct oDefaultCoreConfig, oAudioDriver, oPaths, oRenderer, oReproduction, oOutput, oOutputDevice;

	oAudioDriver["Driver"]             = "Portaudio";
	oAudioDriver["Device"]             = "default";
	oDefaultCoreConfig["Audio driver"] = oAudioDriver;

	if( !voDefaultSearchPaths.empty( ) )
	{
		for( int idx = 0; idx < voDefaultSearchPaths.size( ); idx++ )
			oPaths["default_path_" + std::to_string( idx )] = voDefaultSearchPaths[idx].string( );
		oDefaultCoreConfig["Paths"] = oPaths;
	}

	oRenderer["Class"]                                      = "Freefield";
	oRenderer["Enabled"]                                    = true;
	oRenderer["Reproductions"]                              = "DefaultTalkthrough";
	oRenderer["SpatialEncodingType"]                        = "Binaural";
	oDefaultCoreConfig["Renderer:DefaultBinauralFreefield"] = oRenderer;

	oReproduction["Class"]                                = "Talkthrough";
	oReproduction["Enabled"]                              = true;
	oReproduction["Name"]                                 = "Default talkthrough reproduction";
	oReproduction["Outputs"]                              = "DefaultOutput";
	oDefaultCoreConfig["Reproduction:DefaultTalkthrough"] = oReproduction;

	oOutput["Devices"]                         = "DefaultStereoChannels";
	oOutput["Description"]                     = "Default stereo channels (headphones)";
	oDefaultCoreConfig["Output:DefaultOutput"] = oOutput;

	oOutputDevice["Type"]                                    = "HP";
	oOutputDevice["Description"]                             = "Default headphone hardware device (two-channels)";
	oOutputDevice["Channels"]                                = "1,2";
	oDefaultCoreConfig["OutputDevice:DefaultStereoChannels"] = oOutputDevice;

	return oDefaultCoreConfig;
}

void CreateVACore( const CVAStruct& oConfig, const bool bRemoteControlMode )
{
	pCore = std::unique_ptr<IVAInterface>( VACore::CreateCoreInstance( oConfig ) );
	pCore->Initialize( );

	if( bRemoteControlMode )
	{
		pServerCoreEventHandler = std::make_unique<CServerCoreEventHandler>( );
		pCore->AttachEventHandler( pServerCoreEventHandler.get( ) );
	}
}
bool EstablishNetworkConnection( const std::string& sServerAddress )
{
	pServer = IVANetServer::Create( );
	pServer->SetCoreInstance( pCore.get( ) );

	std::string sServer;
	int iPort;
	SplitServerString( sServerAddress, sServer, iPort );
	if( pServer->Initialize( sServer, iPort ) )
	{
		CVAVersionInfo ver;
		pCore->GetVersionInfo( &ver );

		std::cout << std::endl;
		VASERVER_INFO( "[{:<20}] Core version: {}", "Version", ver.ToString( ) );
		VASERVER_INFO( "[{:<20}] Successfully started and listening on: {}:{}", "Network init", sServer, iPort );
		std::cout << std::endl;
		return true;
	}
	else
	{
		VASERVER_CRITICAL( "[{:<20}] Failed to initialize network communication", "Network init" );
		return false;
	}
}



void WaitForShutdownRequest( const CVAServerArguments& oStartArguments, IHTA::Instrumentation::LoggerRegistry::SinkContainer& oLoggerSinks )
{
	if( oStartArguments.bRemoteControlMode )
	{
		while( !abShutdown )
		{
			std::this_thread::sleep_for( std::chrono::milliseconds( 250 ) );
		}
	}
	else
	{
		while( !abShutdown )
		{
			ControlKeyboardInput( oLoggerSinks );
		}
	}
}
void ControlKeyboardInput( IHTA::Instrumentation::LoggerRegistry::SinkContainer& oLoggerSinks )
{
	const int CTRL_C = 3;
	const int CTRL_D = 4;
	int c            = getch( ); // Note: this is a blocking call!

	oVACoreObjectsMutex.lock();

	if( abShutdown )
	{
		oVACoreObjectsMutex.unlock();
		return;
	}

	if( c == CTRL_C || c == CTRL_D || c == 'q' )
	{
		abShutdown = true;
	}

	if( (char)c == 'm' )
	{
		bool bMuted = pServer->GetCoreInstance( )->GetOutputMuted( );
		VASERVER_INFO( "[{:<20}] {} global output", "Control", ( !bMuted ? "Muting" : "Unmuting" ) );
		pServer->GetCoreInstance( )->SetOutputMuted( !bMuted );
	}

	if( (char)c == '0' )
	{
		VASERVER_INFO( "[{:<20}] Setting output gain to 0 dB", "Control" );
		pServer->GetCoreInstance( )->SetOutputGain( 1.0 );
	}

	if( (char)c == '+' )
	{
		double dGain = pServer->GetCoreInstance( )->GetOutputGain( );
		dGain *= 1.4125;
		VASERVER_INFO( "[{:<20}] Setting output gain to {}", "Control", ratio_to_db20_str( dGain ) );
		pServer->GetCoreInstance( )->SetOutputGain( dGain );
	}

	if( (char)c == '-' )
	{
		double dGain = pServer->GetCoreInstance( )->GetOutputGain( );
		dGain /= 1.4125;
		VASERVER_INFO( "[{:<20}] Setting output gain to {}", "Control", ratio_to_db20_str( dGain ) );
		pServer->GetCoreInstance( )->SetOutputGain( dGain );
	}

	if( (char)c == 'l' )
	{
		CVAStruct oArgs, oReturn, oNewArgs;
		oArgs["getloglevel"]    = true;
		oReturn                 = pServer->GetCoreInstance( )->CallModule( "VACore", oArgs );
		int iCurrentLogLevel    = oReturn["loglevel"];
		int iNewLogLevel        = int( iCurrentLogLevel + 1 ) % 6;
		oNewArgs["setloglevel"] = iNewLogLevel;
		oReturn                 = pServer->GetCoreInstance( )->CallModule( "VACore", oNewArgs );

		oLoggerSinks.console_sink->set_level( ConvertLogLevel( iNewLogLevel ) );
		VASERVER_INFO( "[{:<20}] Switched to log level:  {}", "Control", IVAInterface::GetLogLevelStr( iNewLogLevel ) );
	}

	if( (char)c == 's' )
	{
		CVAStruct oArgs, oReturn;
		oArgs["getsearchpaths"] = true;
		oReturn                 = pServer->GetCoreInstance( )->CallModule( "VACore", oArgs );

		CVAStruct& oSearchPaths( oReturn["searchpaths"] );
		if( oSearchPaths.IsEmpty( ) )
		{
			VASERVER_INFO( "[{:<20}] No search paths added, yet", "Control" );
		}
		else
		{
			VASERVER_INFO( "[{:<20}] Listing all search paths on server side:", "Control" );
			CVAStruct::const_iterator cit = oSearchPaths.Begin( );
			while( cit != oSearchPaths.End( ) )
			{
				CVAStructValue oValue( cit->second );
				cit++;
				if( oValue.IsString( ) )
				{
					VASERVER_INFO( "[{:<20}]  + '{}'", "Control", std::string( oValue ) );
				}
			}
		}
	}

	if( (char)c == 'r' )
	{
		VASERVER_INFO( "[{:<20}] Resetting server manually", "Control" );
		pServer->GetCoreInstance( )->Reset( );
	}

	if( (char)c == 'c' )
	{
		if( !pServer->IsClientConnected( ) )
		{
			VASERVER_INFO( "[{:<20}] Nothing yet, still waiting for connections", "Control" );
		}
		else
		{
			const int iNumClients = pServer->GetNumConnectedClients( );
			if( iNumClients == 1 )
			{
				VASERVER_INFO( "[{:<20}] One client connected", "Control" );
			}
			else
			{
				VASERVER_INFO( "[{:<20}] Counting {} connections", "Control", iNumClients );
			}

			for( int i = 0; i < iNumClients; i++ )
			{
				VASERVER_INFO( "[{:<20}]   +    {}", "Control", pServer->GetClientHostname( i ) );
			}
		}
	}

	oVACoreObjectsMutex.unlock();
}
spdlog::level::level_enum ConvertLogLevel( int iLevel )
{
	if( iLevel < 0 )
		VA_EXCEPT2( INVALID_PARAMETER, "Log level cannot be negative, zero will put VACore to quite mode." );

	auto eNewLevel = spdlog::level::level_enum::off;

	// This is for the console sink, it should always be on info at least
	if( iLevel <= IVAInterface::ErrorLevel::VA_LOG_LEVEL_INFO )
	{
		eNewLevel = spdlog::level::level_enum::info;
	}
	else if( iLevel == IVAInterface::ErrorLevel::VA_LOG_LEVEL_VERBOSE )
	{
		eNewLevel = spdlog::level::level_enum::debug;
	}
	else if( iLevel == IVAInterface::ErrorLevel::VA_LOG_LEVEL_TRACE )
	{
		eNewLevel = spdlog::level::level_enum::trace;
	}

	return eNewLevel;
}

void StopServer( )
{
	try
	{
		oVACoreObjectsMutex.lock();

		if( pServerCoreEventHandler  && pCore )
		{
			pCore->DetachEventHandler( pServerCoreEventHandler.get( ) );
			pServerCoreEventHandler = nullptr;
		}

		if( pServer )
		{
			pServer = nullptr;
		}

		if( pCore )
		{
			pCore->Finalize( );
			pCore = nullptr;
		}
		
		oVACoreObjectsMutex.unlock();
	}
	catch( CVAException& e )
	{
		VASERVER_CRITICAL( "[{:<20}] {}", "Exception", e );

		if( pCore )
		{
			pCore->Finalize( );
			pCore = nullptr;
		}

		exit( 255 );
	}
	catch( ... )
	{
		if( pCore )
		{
			pCore->Finalize( );
			pCore = nullptr;
		}

		VASERVER_CRITICAL( "[{:<20}] An unknown error occured", "Exception" );
		exit( 255 );
	}
}